import requests

token = 'glpat-QPdhx6yMunMtPpvHvuXs'
base_url = 'https://gitlab.com/api/v4'

# Авторизация
headers = {'Authorization': f'Bearer {token}'}

# Запрос на список коммитов
repo_id = '55045769'
branch = 'main'
date = '2022-01-01'  # Замените на нужную дату

params = {
    'ref_name': branch,
    'since': date
}

url = f'{base_url}/projects/{repo_id}/repository/commits'

response = requests.get(url, headers=headers, params=params)

file = open('Список коммитов.txt', 'w')
if response.status_code == 200:
    commits = response.json()
    for i in range(0, len(commits)):
        file.write('\n' + str(i+1) + '\n')
        for f in commits[i].items():
            file.write(str(f) + '\n')
    print('Список коммитов успешно получен и записан в Список коммитов.txt')
else:
    print('Ошибка при получении списка коммитов')
    print(f'Код ошибки: {response.status_code}')
    print(f'Ответ сервера: {response.text}')